
function hasClass(el, cls) {
    return el.className && new RegExp("(\\s|^)" + cls + "(\\s|$)").test(el.className);
}

function init() {



   if(!Modernizr.csstransitions) {

        var container = document.getElementById('wheel');
        var inner = document.getElementById('inner');


        for(i=0;i<container.children.length;i++) {

            if(hasClass(container.children[i],"left-arrow")) {
                container.children[i].addEventListener("click",function() {
                    Velocity(inner, { left: "130px" }, { duration: 1000 });
                });
            }
            if(hasClass(container.children[i],"right-arrow")) {
                container.children[i].addEventListener("click",function() {
                    Velocity(inner, { left: "-450px" }, { duration: 1000 });
                });
            }
        }


   }




}

document.addEventListener("DOMContentLoaded", function() {
    init();
});