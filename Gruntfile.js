module.exports = function (grunt) {

    var LOCAL_PATH = 'dev';
    var RELEASE_PATH = 'live';

    global.outputPath = "dev";

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: ['<%= grunt.option(\"target\") %>/app/','.sass-cache'],
        concat: {
            css: {

                // the files to concatenate
                src: [
                    //'src/bower_components/foundation/css/foundation.css',
                    //'src/bower_components/angular-material/angular-material.min.css',
                    'src/assets/css/foundation.css'
                ],
                // the location of the resulting CSS file
                dest: '<%= grunt.option(\"target\") %>/app/assets/css/main.css',
                nonull: true

            },
            index: {
                options: {
                    // define a string to put between each file in the concatenated output
                    separator: ';\n',
                    stripBanners: true
                },
                src: [
                    //'bower_components/html5shiv/dist/html5shiv.min.js'
                    'bower_components/velocity/velocity.min.js',
                    'src/assets/js/main.js'
                ],
                // the location of the resulting JS file
                dest: '<%= grunt.option(\"target\") %>/app/assets/js/index.js',
                nonull: true
            }

        },
        compass: {
            dist: {
                options: {
                    sassDir: 'src/sass',
                    cssDir: 'src/assets/css'
                }
            }
        },


        cssmin: {
            css: {
                src: '<%= grunt.option(\"target\") %>/app/assets/css/main.css',
                dest: '<%= grunt.option(\"target\") %>/app/assets/css/main.css'
            }
        },

        copy: {
            main: {
                files: [{
                    nonull: true,
                    cwd: 'src/',
                    expand: true,
                    src: [
                        "index.html"],
                    dest: "<%= grunt.option(\"target\") %>/app/"
                },
                    {
                        cwd: 'src/assets/',
                        nonull: true,
                        expand: true,
                        src: [
                            "img/**"],
                        dest: "<%= grunt.option(\"target\") %>/app/assets/"
                    },
                    {
                        cwd: 'src/assets/',
                        nonull: true,
                        expand: true,
                        src: [
                            "js/modernizr.js"],
                        dest: "<%= grunt.option(\"target\") %>/app/assets/"
                    },
                    {
                        cwd: 'src/assets/',
                        nonull: true,
                        expand: true,
                        src: [
                            "fonts/**"],
                        dest: "<%= grunt.option(\"target\") %>/app/assets/"
                    },
                    {
                        cwd: 'src/assets/',
                        nonull: true,
                        expand: true,
                        src: [
                            "css/**"],
                        dest: "<%= grunt.option(\"target\") %>/app/assets/"
                    }]
            }
        },
        watch: {
            options: {
                livereload: true
            },
            html: {
                files: ['src/**'],
                tasks: ['sass','concat', 'cssmin', 'copy']
            }

        },
        connect: {
            server: {
                options: {
                    port: 9001,
                    hostname: 'localhost',
                    base: '<%= grunt.option(\"target\") %>/app',
                    keepalive: true
                }
            }
        },
        htmlmin: {                                     // Task
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                expand: true,
                cwd: '<%= grunt.option(\"target\") %>/app',
                src: ['**/*.html'],
                dest: '<%= grunt.option(\"target\") %>/app/'
            }
        },
        imagemin: {                          // Task
            dynamic: {                         // Another target
                files: [{
                    expand: true,                  // Enable dynamic expansion
                    cwd: 'src/assets/img',                   // Src matches are relative to this path
                    src: ['**/*.{png,jpg,gif}'],   // Actual patterns to match
                    dest: '<%= grunt.option(\"target\") %>/app/assets/img'                  // Destination path prefix
                }]
            }
        },
        modernizr: {

            dist: {

                "dest" : "src/assets/js/modernizr.js",
                "cache" : false,
                "crawl" : true,
                "options" : [
                    "setClasses",
                    ],
                "tests" : ['csstransitions'],
                "files" : {
                    "src": [
                        "*[^(g|G)runt(file)?].{js,css,scss}",
                        "**[^node_modules]/**/*.{js,css,scss}",
                        "!lib/**/*"
                    ]
                }
            }
        }



    });

    //grunt tasks
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-css');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks("grunt-modernizr");

    grunt.registerTask("setOutput", "Set the output folder for the build.", function () {
        if (global.buildType === "local") {
            global.outputPath = LOCAL_PATH;
        }
        if (global.buildType === "release") {
            global.outputPath = RELEASE_PATH;
        }
        if (grunt.option("target")) {
            global.outputPath = grunt.option("target");
        }

        grunt.option("target", global.outputPath);
        grunt.log.writeln("Output path: " + grunt.option("target"));
    });


    grunt.registerTask('dev', function() {
        global.outputPath = "dev";
        grunt.task.run(['setOutput', 'clean', 'compass', 'concat', 'copy']);
    });

    grunt.registerTask('preview', function() {
        global.outputPath = "dev";
        grunt.task.run(['setOutput', 'clean', 'compass', 'concat', 'copy', 'htmlmin','imagemin']);
    });

    grunt.registerTask('release', function() {
        global.outputPath = "dev";
        grunt.task.run(['setOutput', 'clean', 'compass', 'concat', 'copy','cssmin', 'htmlmin','imagemin']);
    });

};
