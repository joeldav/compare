#config

http_path = "/"
css_dir = "assets/css"
sass_dir = "sass"
images_dir = "assets/img"
javascripts_dir = "assets/js"
extensions_dir = "bower_components"